
package Automat;

import java.util.Scanner;
import java.util.InputMismatchException;

public class Automat
{	
	private static int menu; // statyczne pola 
	private static int wybor;
	private static int dodatki;
	private static int metoda;
	protected boolean koniec;

	public static void main(String[] args)
    {   					
		Scanner in = new Scanner(System.in); // tworzenie obiektu in klasy Scanner zimportowanej z bibliotek javy, jest odpowiedzialna za wczytywanie znakow z klawiatury
	
		Produkty[] produkty = new Produkty[5]; // interface Prodykty w sklad, ktorego wchodza inne klasy jak Gazowane, Woda itd.
		
		produkty[0] = new Gazowane(); /* tworzenie obiektu klasy Gazowane */
		produkty[1] = new Woda(); /* tworzenie obiektu Klasy Woda */
		produkty[2] = new Przekaski(); /* tworzenie obiektu Klasy Przekaski */
		produkty[3] = new Kawa(); /* tworzenie obiektu Klasy Kawa */
		produkty[4] = new Herbata(); /* tworzenie obiektu Klasy Herbata */
			
        boolean koniec = false;
        
        do {   // dzialanie całego programu w pętli 
        	  Menu.menu(); // menu głowne AUTOMATU

            try // wychwytywanie błedu w tym przypadku input wczytywanie z klawiatury nie dozwolonych znaków jak litery i inne znaki 
  			{
  	      	menu = in.nextInt(); // przypisywanie zmiennej statycznej menu warotści wczytanej z klawiatury
  
              switch (menu) {
              case 1:
            	  Menu.menuProdukty(menu); // menu z produktami
            	  wybor = in.nextInt();
            	  if(wybor==1 || wybor==2 || wybor==3)
                  {	
            		  Menu.menuWybor(wybor);
            		  Platnosc.menuPlatnosci();
            		  Platnosc.metodaPlatnosci(metoda = in.nextInt());
            		  produkty[0].kupNapoj(wybor, metoda, produkty[0].pobierzGotowke(in)); // odwoływanie się do metody kupNapoj klasy Gazowane i przypisywanie jej wartości wczytanych z klawiatury		  
                  }
            	  
            	  else
            		  System.out.println(" ZLY WYBOR ");
                    break;
              case 2:
            	  Menu.menuProdukty(menu);
            	  wybor = in.nextInt();
            	  if(wybor==4 || wybor==5)
                  {	
            		  Menu.menuWybor(wybor);
            		  Platnosc.menuPlatnosci();
            		  Platnosc.metodaPlatnosci(metoda = in.nextInt());
            		  produkty[1].kupWode(wybor, metoda, produkty[1].pobierzGotowke(in));		  
                  }
            	  
            	  else
            		  System.out.println(" ZLY WYBOR ");
                    break;	
              case 3:
            	  Menu.menuProdukty(menu);
            	  wybor = in.nextInt();
            	  if(wybor==6 || wybor==7 || wybor==8)
                  {	
            		  Menu.menuWybor(wybor);
            		  Platnosc.menuPlatnosci();
            		  Platnosc.metodaPlatnosci(metoda = in.nextInt());
            		  produkty[2].kupPrzekaski(wybor, metoda, produkty[2].pobierzGotowke(in));
                  }
            	  
            	  else
            		  System.out.println(" ZLY WYBOR ");
                    break;
              case 4:
            	  Menu.menuProdukty(menu);
            	  wybor = in.nextInt();
            	  if(wybor==9 || wybor==10 || wybor==11)
                  {	
            		  Menu.menuWybor(wybor);
            		  Menu.menuDodatkiDoKawy();
            		  dodatki = in.nextInt();	  
            		  if(dodatki==1 || dodatki==2 || dodatki==3 || dodatki==4)
            		  {
            			  Menu.menuWyborDodatkowDoKawy(dodatki);
            			  Platnosc.menuPlatnosci();
            			  Platnosc.metodaPlatnosci(metoda = in.nextInt());
            			  produkty[3].kupKawe(wybor, dodatki, metoda, produkty[3].pobierzGotowke(in)); // kwota kawa/herba, dodatki
            		  }
            		  
            		  else 
            		  System.out.println(" Nie ma dodatkow ");  		  
                  }
            	 
            	  else
            		  System.out.println(" ZLY WYBOR ");
                    break;
              case 5:      	  
            	  Menu.menuProdukty(menu);
            	  wybor = in.nextInt();  
            	  if(wybor==12 || wybor==13)
                  {	
            		  Menu.menuWybor(wybor);
            		  Menu.menuDodatkiDoHerbaty();
            		  dodatki = in.nextInt();	  
            		  if(dodatki==1 || dodatki==2 || dodatki==3 || dodatki==4)
            		  {
            			  Menu.menuWyborDodatkowDoHerbaty(dodatki);
            			  Platnosc.menuPlatnosci();
            			  Platnosc.metodaPlatnosci(metoda = in.nextInt());
            			  produkty[4].kupHerbate(wybor, dodatki, metoda, produkty[4].pobierzGotowke(in));
            		  }
            		  else 
            		  System.out.println(" Nie ma dodatkow ");
                  }
            	  
            	  else
            		  System.out.println(" ZLY WYBOR ");
                    break;
              case 6:
            	   Menu.menuInformacje();
              case 0:
                    koniec = true;
                    break;
              default:
                    System.out.println(" Brak wyboru ");
              }     
  			}
    			
            catch (InputMismatchException e) // wychwytywanie bledow SCANNER INPUT kiedy zamiast liczb od 0 do 9 podamy litere, lub znak wyskoczy blad. Program wychwytuje błąd, ale nie kończy działania i dalej działa w pętli 
      		{
      			System.err.println(" Błąd: Złe dane wejściowe \"" + in.nextLine() + "\" można używaż jedynie liczb od 0 do 9: ");
      			System.out.println(" SPROBUJ JESZCZE RAZ -->");
      			main(args);
      		}
        } while (!koniec); // koniek pętli
        
        System.out.println("		Miłego Dnia :D ");
        System.gc(); 	
    }
}