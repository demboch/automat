
package Automat;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu
{	
	Scanner in = new Scanner(System.in);
	
	public static void menu()
	{
		ArrayList<String> menu = new ArrayList<String>(); // kolekcje 

		menu.add("");
		menu.add("     **************************************** ");
		menu.add("     *                 MENU                 * ");
		menu.add("     **************************************** ");
		menu.add("     *        1. Napoje Gazowane            * ");
		menu.add("     *        2. Woda                       * ");
		menu.add("     *        3. Przekaski                  * ");
		menu.add("     *        4. Kawa                       * ");
		menu.add("     *        5. Herbata                    * ");
		menu.add("     *        6. Informacje                 * ");
		menu.add("     *        0. Koniec                     * ");
		menu.add("     ****************************************\n ");
		menu.add("WYBIERZ KATEGORIE: ");

		for(String s : menu) { System.out.println(s); }
    }
		
	public static void menuProdukty(int menu)
	{
		ArrayList<String> produkty = new ArrayList<String>();
		produkty.add("WYBIERZ PRODUKT: ");
		switch(menu)
			{
			case 1:	
				produkty.add(" 1 - Cola 2 zl ");
				produkty.add(" 2 - Fanta 2 zl ");
	    	  	produkty.add(" 3 - Sprite 2 zl ");
	    	  	break;			
			case 2:
				produkty.add(" 4 - Woda Mineralna 1.50 zl ");
				produkty.add(" 5 - Woda Gazowana 1.50 zl ");
	    	  	break;	
			case 3:	
				produkty.add(" 6 - Batony 2.50 zl ");
				produkty.add(" 7 - Chipsy 3 zl ");
				produkty.add(" 8 - Paluszki 2.50 zl ");
				break;
			case 4:
				produkty.add(" 9 - Espresso 1.50 zl ");
				produkty.add(" 10 - Cappuccino 1.50 zl ");
				produkty.add(" 11 - Latte Macchiato 1.50 zl ");
	    	  	break;
			case 5:
				produkty.add(" 12 - Herbata 1.50 zl ");
				produkty.add(" 13 - Herbata Owocowa 1.50 zl ");
	    	  	break;
	       default:
	    	    produkty.add("BRAK WYBORU");
			}
		for(String s : produkty) { System.out.println(s); }
	}
	
	public static void menuWybor(int wybor)
	{
		ArrayList<String> wybrane = new ArrayList<String>();
		
		switch(wybor)
			{
			case 1:
				wybrane.add(" Wybrales Cole \n");
	    	  	break;
			case 2:
				wybrane.add(" Wybrales Fante \n");
	    	  	break;
			case 3:
				wybrane.add(" Wybrales Sprite \n");
	    	  	break;
			case 4:
				wybrane.add(" Wybrales Wode Mineralna \n");
				break;
			case 5:
				wybrane.add(" Wybrales Wode Gazowana \n");
	    	  	break;
			case 6:
				wybrane.add(" Wybrales Batony \n");
	    	  	break;
			case 7:
				wybrane.add(" Wybrales Chipsy \n");
	    	  	break;
			case 8:
				wybrane.add(" Wybrales Paluszki \n");
	    	  	break;
			case 9:
				wybrane.add(" Wybrales Espresso \n");
	    	  	break;
			case 10:
				wybrane.add(" Wybrales Cappuccino \n");
	    	  	break;
			case 11:
				wybrane.add(" Wybrales Latte Macchiato \n");
	    	  	break;
			case 12:
				wybrane.add(" Wybrales Herbate \n");
	    	  	break;
			case 13:
				wybrane.add(" Wybrales Herbate Owocowa \n");
	    	  	break;
			}
		
		for(String s : wybrane) { System.out.println(s); }
	}
	
	public static void menuDodatkiDoKawy()
	{
		ArrayList<String> dodatkiKawy = new ArrayList<String>();
		
		dodatkiKawy.add("WYBIERZ DODATKI DO KAWY: ");
		dodatkiKawy.add(" 1 - Bez dodatkow ");
		dodatkiKawy.add(" 2 - Mleko 50 gr ");
		dodatkiKawy.add(" 3 - Cukier 50 gr ");
		dodatkiKawy.add(" 4 - Mleko + Cukier 1 zl ");
		
		for(String s : dodatkiKawy) { System.out.println(s); }
	}
	
	public static void menuWyborDodatkowDoKawy(int dodatki)
	{
		ArrayList<String> wyborDodatkowKawy = new ArrayList<String>();
		
		switch(dodatki)
			{
			case 1:	
				wyborDodatkowKawy.add(" Kawa bez dodatkow \n");
	    	  	break;			
			case 2:
				wyborDodatkowKawy.add(" Kawa z Mlekiem 2 zl \n");
	    	  	break;
			case 3:
				wyborDodatkowKawy.add(" Kawa z Cukrem 2 zl \n");
	    	  	break;	
			case 4:	
				wyborDodatkowKawy.add(" Kawa z Mlekiem i Cukrem 2.50 zl \n");
				break;
	       default:
	    	    wyborDodatkowKawy.add("BRAK WYBORU");
			}
		
		for(String s : wyborDodatkowKawy) { System.out.println(s); }
	}
	
	public static void menuDodatkiDoHerbaty()
	{
		ArrayList<String> dodatkiHerbaty = new ArrayList<String>();
		
		dodatkiHerbaty.add("WYBIERZ DODATKI DO HERBATY: ");
		dodatkiHerbaty.add(" 1 - Bez dodatkow ");
		dodatkiHerbaty.add(" 2 - Cytryna 50 gr ");
		dodatkiHerbaty.add(" 3 - Cukier 50 gr ");	
		dodatkiHerbaty.add(" 4 - Cukier + Cytryna 1 zl ");
		
		for(String s : dodatkiHerbaty) { System.out.println(s); }
	}
	
	public static void menuWyborDodatkowDoHerbaty(int dodatki)
	{
		ArrayList<String> wyborDodatkowHerbaty = new ArrayList<String>();
		
		switch(dodatki)
			{
			case 1:	
				wyborDodatkowHerbaty.add(" Herbata bez dodatkow \n");
	    	  	break;			
			case 2:
				wyborDodatkowHerbaty.add(" Herbata z Cytryna 2 zl \n");
	    	  	break;
			case 3:
				wyborDodatkowHerbaty.add(" Herbata z Cukrem 2 zl \n");
	    	  	break;	
			case 4:	
				wyborDodatkowHerbaty.add(" Herbata z Cukrem i Cytryna 2.50 zl \n");
				break;
	       default:
	    	    wyborDodatkowHerbaty.add("BRAK WYBORU");
			}
		
		for(String s : wyborDodatkowHerbaty) { System.out.println(s); }
	}
	
	public static void menuInformacje()
	{
		ArrayList<String> informacje = new ArrayList<String>();
		
		informacje.add("     **************************************** ");
		informacje.add("     *              INFORMACJE              * ");
		informacje.add("     **************************************** ");
		informacje.add("     *  Nazwa: Necta & Wittenborg  FB 7600  * ");
		informacje.add("     *  Numer seryjny: 1012002119           * ");
		informacje.add("     *                                      * ");
		informacje.add("     *  Kontakt: infolinia +48 856 712 555  * ");
		informacje.add("     *	         serwis    +48 608 921 055  * ");
		informacje.add("     *                                      * ");  
		informacje.add("     *  Ideal Group Sp z o.o.               * ");
		informacje.add("     *  ul. Pogodna 63/1                    * ");
		informacje.add("     *  15-365 Bia�ystok                    * ");
		informacje.add("     *                                      * ");
		informacje.add("     **************************************** ");
		    
		for(String s : informacje) { System.out.println(s); }
	}
	
    public static int pobierzMenu(Scanner in) {
    	return in.nextInt(); 
    }
    
    public void ustawMenu(Scanner in) {
		this.in = in;
    }
	
	public static int pobierzWybor(Scanner in) {
		return in.nextInt();
	}

	public void ustawWybor(Scanner in) {
		this.in = in;
	}
	
	public static int pobierzDodatki(Scanner in) {
		return in.nextInt();
	}

	public void ustawDodatki(Scanner in) {
		this.in = in;;
	}
}


