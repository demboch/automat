
package Automat;

import java.util.ArrayList;

public class Przekaski extends Platnosc implements Produkty
{
    private int iloscBatonow;
    private double cenaBatonow;
    private int iloscChipsow;
    private double cenaChipsow;
    private int iloscPaluszkow;
    private double cenaPaluszkow;
        
        public Przekaski() // konstruktor domyślny, bezparametrowy
        {
        	iloscBatonow = 15;
        	cenaBatonow = 2.5;
        	iloscChipsow = 10;
        	cenaChipsow = 3;
        	iloscPaluszkow = 5;
        	cenaPaluszkow = 2.5;
        }
        
        public Przekaski(int iloscBatonow, double cenaBatonow, int iloscChipsow, double cenaChipsow, int iloscPaluszkow, double cenaPaluszkow) // konstruktor przyjmujący jeden, lub wiele parametrow
        {
        	this.iloscBatonow = iloscBatonow;
        	this.cenaBatonow = cenaBatonow;
        	this.iloscChipsow = iloscChipsow;
        	this.cenaChipsow = cenaChipsow;
        	this.iloscPaluszkow = iloscPaluszkow;
        	this.cenaPaluszkow = cenaPaluszkow;
        }
        
        public Przekaski(Przekaski przekaski) // konstruktor przyjmujący obiekt przekaski klasy Przekaski jako parametr
        {
        	iloscBatonow = przekaski.iloscBatonow;
        	cenaBatonow = przekaski.cenaBatonow;
        	iloscChipsow = przekaski.iloscChipsow;
        	cenaChipsow = przekaski.cenaChipsow;
        	iloscPaluszkow = przekaski.iloscPaluszkow;
        	cenaPaluszkow = przekaski.cenaPaluszkow;
        }
        
        public final double kupPrzekaski(int przekaski, int metoda, double gotowka) // metoda odpowiedzialna za kupowanie danego produktu
        {   
        	ArrayList<String> Przekaski = new ArrayList<String>();
        	
        	if(iloscBatonow>0 && przekaski==6 && metoda==1 || metoda==2)
            {
        		switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaBatonow)
	            		{
	            			gotowka=gotowka-cenaBatonow;            
	            			Przekaski.add(" Reszta: " + gotowka + " zl");
	            			iloscBatonow=iloscBatonow - 1;
	            			Przekaski.add(" Ilosc Batonow w Automacie: " + iloscBatonow + "\n");
	            			Przekaski.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaBatonow)
	            		{
	            			karta=karta-cenaBatonow;				            
	            			Przekaski.add(" Srodki na kacie po zakupie Batona wynosza: " + karta + " zl");
	            			iloscBatonow=iloscBatonow - 1;
	            			Przekaski.add(" Ilosc Batonow w Automacie: " + iloscBatonow + "\n");
	            			Przekaski.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
            }
               
        	else if(iloscChipsow>0 && przekaski==7 && metoda==1 || metoda==2)
            {
        		switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaChipsow)
	            		{
	            			gotowka=gotowka-cenaChipsow;            
	            			Przekaski.add(" Reszta: " + gotowka + " zl");	
	            			iloscChipsow=iloscChipsow - 1;
	            			Przekaski.add(" Ilosc Chipsow w Automacie: " + iloscChipsow + "\n");
	            			Przekaski.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break;  
	            	case 2:
	            		if(karta>=cenaChipsow)
	            		{
	            			karta=karta-cenaChipsow;				            
	            			Przekaski.add(" Srodki na kacie po zakupie Chipsow wynosza: " + karta + " zl");
	            			iloscChipsow=iloscChipsow - 1;
	            			Przekaski.add(" Ilosc Chipsow w Automacie: " + iloscChipsow + "\n");
	            			Przekaski.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
            }
        	
        	else if(iloscPaluszkow>0 && przekaski==8 && metoda==1 || metoda==2)
            {
        		switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaPaluszkow)
	            		{
	            			gotowka=gotowka-cenaPaluszkow;            
	            			Przekaski.add(" Reszta: " + gotowka + " zl");
	            			iloscPaluszkow=iloscPaluszkow - 1;
	            			Przekaski.add(" Ilosc Paluszkow w Automacie: " + iloscPaluszkow + "\n");
	            			Przekaski.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break;
	            	case 2:
	            		if(karta>=cenaPaluszkow)
	            		{
	            			karta=karta-cenaPaluszkow;				            
	            			Przekaski.add(" Srodki na kacie po zakupie Paluszkow wynosza: " + karta + " zl");
	            			iloscPaluszkow=iloscPaluszkow - 1;
	            			Przekaski.add(" Ilosc Paluszkow w Automacie: " + iloscPaluszkow + "\n");
	            			Przekaski.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
            }
        	
            else
            	Przekaski.add(" Brak Towaru, lub nie wybrano metody platnosci ");
        	
        	for(String s : Przekaski) { System.out.println(s); }
                return gotowka;     
        }

		public double kupWode(int woda, int metoda, double gotowka) {
			return 0;
		}

		public double kupKawe(int kawa, int dodatki, int metoda, double gotowka) {
			return 0;
		}

		public double kupHerbate(int herbata, int dodatki, int metoda, double gotowka) {
			return 0;
		}
		
		public double kupNapoj(int napoj, int metoda, double gotowka) {
			return 0;
		}
		
		public double wciaganiePieniedzy(double gotowka) {
			return 0;
		}
}