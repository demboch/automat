
package Automat;

import java.util.Scanner;

public interface Produkty // klasa abstrakcyjna interface zawierająca puste metody przyjmujące kilka parametrów
{
	public double kupWode(int woda, int metoda, double gotowka);
	
	public double kupNapoj(int napoj, int metoda, double gotowka);
	
	public double kupKawe(int kawa, int dodatki, int metoda, double gotowka);
	
	public double kupHerbate(int herbata, int dodatki, int metoda, double gotowka);
	
	public double kupPrzekaski(int przekaski, int metoda, double gotowka);
	
	public double wciaganiePieniedzy(double gotowka);

	public double pobierzGotowke(Scanner in);
}


/*
klasa abstrakcyjna zawiera puste metody klas pochodnych

abstract class Napoje 
{
	public abstract double kupWode();
	
	public abstract double kupNapoj();
	
	public abstract double kupKawe();
	
	public abstract double kupHerbate();
}

*/
