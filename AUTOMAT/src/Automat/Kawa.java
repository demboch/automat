
package Automat;

import java.util.ArrayList;

public class Kawa extends Platnosc implements Produkty 
{
	private int iloscEspresso;
    private double cenaEspresso;
    private int iloscCappuccino;
    private double cenaCappuccino;
    private int iloscLatteMacchiato;
    private double cenaLatteMacchiato;
        
    private double cenaCukru;
    private double cenaMleka;
    private int cenaMleka_Cukru;
        
        public Kawa() // konstruktor domyślny, bezparametrowy
        {
        	iloscEspresso = 6;
            cenaEspresso = 1.5;
            iloscCappuccino = 6;
            cenaCappuccino = 1.5;
            iloscLatteMacchiato = 6;
            cenaLatteMacchiato = 1.5;
            cenaCukru = 0.5;
            cenaMleka = 0.5;
            cenaMleka_Cukru = 1;
        }
        
        public Kawa(int iloscEspresso, double cenaEspresso, int iloscCappuccino, double cenaCappuccino, int iloscLatteMacchiato, double cenaLatteMacchiato, double cenaCukru, double cenaMleka, int cenaMleka_Cukru) // konstruktor przyjmujący jeden, lub wiele parametrow
        {
        	this.iloscEspresso = iloscEspresso;
        	this.cenaEspresso = cenaEspresso;
        	this.iloscCappuccino = iloscCappuccino;
        	this.cenaCappuccino = cenaCappuccino;
        	this.iloscLatteMacchiato = iloscLatteMacchiato;
        	this.cenaLatteMacchiato = cenaLatteMacchiato;
            this.cenaCukru = cenaCukru;
            this.cenaMleka = cenaMleka;
            this.cenaMleka_Cukru = cenaMleka_Cukru;
        }
        
        public Kawa(Kawa kawa) // konstruktor przyjmujący obiekt kawa klasy Kawa jako parametr
        {
        	iloscEspresso = kawa.iloscEspresso;
            cenaEspresso = kawa.cenaEspresso;
            iloscCappuccino = kawa.iloscCappuccino;
            cenaCappuccino = kawa.cenaCappuccino;
            iloscLatteMacchiato = kawa.iloscLatteMacchiato;
            cenaLatteMacchiato = kawa.cenaLatteMacchiato;
            cenaCukru = kawa.cenaCukru;
            cenaMleka = kawa.cenaMleka;
            cenaMleka_Cukru = kawa.cenaMleka_Cukru;
        }
	
	    public final double kupKawe(int kawa, int dodatki, int metoda, double gotowka) // metoda odpowiedzialna za kupowanie danego produktu
	    {
	    	ArrayList<String> Kawa = new ArrayList<String>();
	    	
	    	if(iloscEspresso>0 && kawa==9 && metoda==1 || metoda==2)
	        {
	    		if(dodatki==1)
	    		{
	                switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaEspresso )
	            		{
	            			gotowka=gotowka-cenaEspresso ;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscEspresso=iloscEspresso - 1;
	                        Kawa.add(" Ilosc Espresso w Automacie: " + iloscEspresso  + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaEspresso )
	            		{
	            			karta=karta-cenaEspresso;				            
	            			Kawa.add(" Srodki na kacie po zakupie Espresso wynosza: " + karta + " zl");
	                        iloscEspresso=iloscEspresso - 1;
	                        Kawa.add(" Ilosc Espresso w Automacie: " + iloscEspresso + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}  
	    		
	    		else if(dodatki==2)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaEspresso+cenaMleka)
	            		{
	            			gotowka=gotowka-cenaEspresso-cenaMleka;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	                        iloscEspresso=iloscEspresso - 1;
	                        Kawa.add(" Ilosc Espresso w Automacie: " + iloscEspresso + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaEspresso+cenaMleka)
	            		{
	            			karta=karta-cenaEspresso-cenaMleka;				            
	            			Kawa.add(" Srodki na kacie po zakupie Espresso wynosza: " + karta + " zl");
	                        iloscEspresso=iloscEspresso - 1;
	                        Kawa.add(" Ilosc Espresso w Automacie: " + iloscEspresso + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	    		
	    		else if(dodatki==3)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaEspresso+cenaCukru)
	            		{
	            			gotowka=gotowka-cenaEspresso-cenaCukru;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	                        iloscEspresso=iloscEspresso - 1;
	                        Kawa.add(" Ilosc Espresso w Automacie: " + iloscEspresso + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaEspresso+cenaCukru)
	            		{
	            			karta=karta-cenaEspresso-cenaCukru;				            
	            			Kawa.add(" Srodki na kacie po zakupie Espresso wynosza: " + karta + " zl");
	                        iloscEspresso=iloscEspresso - 1;
	                        Kawa.add(" Ilosc Espresso w Automacie: " + iloscEspresso + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	            else if(dodatki==4)
	            {
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaEspresso+cenaMleka_Cukru)
	            		{
	            			gotowka=gotowka-cenaEspresso-cenaMleka_Cukru;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	                        iloscEspresso=iloscEspresso - 1;
	                        Kawa.add(" Ilosc Espresso w Automacie: " + iloscEspresso + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaEspresso+cenaMleka_Cukru)
	            		{
	            			karta=karta-cenaEspresso-cenaMleka_Cukru;				            
	            			Kawa.add(" Srodki na kacie po zakupie Espresso wynosza: " + karta + " zl");
	                        iloscEspresso=iloscEspresso - 1;
	                        Kawa.add(" Ilosc Espresso w Automacie: " + iloscEspresso + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	            }
	        }
	    	
	    	else if(iloscCappuccino>0 && kawa==10 && metoda==1 || metoda==2)
	        {
	    		if(dodatki==1)
	    		{
	                switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaCappuccino)
	            		{
	            			gotowka=gotowka-cenaCappuccino;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscCappuccino=iloscCappuccino - 1;
	                        Kawa.add(" Ilosc Cappuccino w Automacie: " + iloscCappuccino + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaCappuccino)
	            		{
	            			karta=karta-cenaCappuccino;				            
	            			Kawa.add(" Srodki na kacie po zakupie Cappuccino wynosza: " + karta + " zl");
	            			iloscCappuccino=iloscCappuccino - 1;
	                        Kawa.add(" Ilosc Cappuccino w Automacie: " + iloscCappuccino + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}  
	    		
	    		else if(dodatki==2)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaCappuccino+cenaMleka)
	            		{
	            			gotowka=gotowka-cenaCappuccino-cenaMleka;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscCappuccino=iloscCappuccino - 1;
	                        Kawa.add(" Ilosc Cappuccino w Automacie: " + iloscCappuccino + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaCappuccino+cenaMleka)
	            		{
	            			karta=karta-cenaCappuccino-cenaMleka;				            
	            			Kawa.add(" Srodki na kacie po zakupie Cappuccino wynosza: " + karta + " zl");
	            			iloscCappuccino=iloscCappuccino - 1;
	                        Kawa.add(" Ilosc Cappuccino w Automacie: " + iloscCappuccino + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	    		
	    		else if(dodatki==3)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaCappuccino+cenaCukru)
	            		{
	            			gotowka=gotowka-cenaCappuccino-cenaCukru;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscCappuccino=iloscCappuccino - 1;
	                        Kawa.add(" Ilosc Cappuccino w Automacie: " + iloscCappuccino + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaCappuccino+cenaCukru)
	            		{
	            			karta=karta-cenaCappuccino-cenaCukru;				            
	            			Kawa.add(" Srodki na kacie po zakupie Cappuccino wynosza: " + karta + " zl");
	            			iloscCappuccino=iloscCappuccino - 1;
	                        Kawa.add(" Ilosc Cappuccino w Automacie: " + iloscCappuccino + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	            else if(dodatki==4)
	            {
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaCappuccino+cenaMleka_Cukru)
	            		{
	            			gotowka=gotowka-cenaCappuccino-cenaMleka_Cukru;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscCappuccino=iloscCappuccino - 1;
	                        Kawa.add(" Ilosc Cappuccino w Automacie: " + iloscCappuccino + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaCappuccino+cenaMleka_Cukru)
	            		{
	            			karta=karta-cenaCappuccino-cenaMleka_Cukru;				            
	            			Kawa.add(" Srodki na kacie po zakupie Cappuccino wynosza: " + karta + " zl");
	            			iloscCappuccino=iloscCappuccino - 1;
	                        Kawa.add(" Ilosc Cappuccino w Automacie: " + iloscCappuccino + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	            }
	        }
	    	
	    	else if(iloscLatteMacchiato>0 && kawa==11 && metoda==1 || metoda==2)
	        {
	    		if(dodatki==1)
	    		{
	                switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaLatteMacchiato)
	            		{
	            			gotowka=gotowka-cenaLatteMacchiato;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscLatteMacchiato=iloscLatteMacchiato - 1;
	                        Kawa.add(" Ilosc Latte Macchiato w Automacie: " + iloscLatteMacchiato + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaLatteMacchiato)
	            		{
	            			karta=karta-cenaLatteMacchiato;				            
	            			Kawa.add(" Srodki na kacie po zakupie Latte Macchiato wynosza: " + karta + " zl");
	            			iloscLatteMacchiato=iloscLatteMacchiato - 1;
	                        Kawa.add(" Ilosc Latte Macchiato w Automacie: " + iloscLatteMacchiato + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}  
	    		
	    		else if(dodatki==2)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaLatteMacchiato+cenaMleka)
	            		{
	            			gotowka=gotowka-cenaLatteMacchiato-cenaMleka;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscLatteMacchiato=iloscLatteMacchiato - 1;
	                        Kawa.add(" Ilosc Latte Macchiato w Automacie: " + iloscLatteMacchiato + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaLatteMacchiato+cenaMleka)
	            		{
	            			karta=karta-cenaLatteMacchiato-cenaMleka;				            
	            			Kawa.add(" Srodki na kacie po zakupie Latte Macchiato wynosza: " + karta + " zl");
	            			iloscLatteMacchiato=iloscLatteMacchiato - 1;
	                        Kawa.add(" Ilosc Latte Macchiato w Automacie: " + iloscLatteMacchiato + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	    		
	    		else if(dodatki==3)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaLatteMacchiato+cenaCukru)
	            		{
	            			gotowka=gotowka-cenaLatteMacchiato-cenaCukru;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscLatteMacchiato=iloscLatteMacchiato - 1;
	                        Kawa.add(" Ilosc Latte Macchiato w Automacie: " + iloscLatteMacchiato + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaLatteMacchiato+cenaCukru)
	            		{
	            			karta=karta-cenaLatteMacchiato-cenaCukru;				            
	            			Kawa.add(" Srodki na kacie po zakupie Latte Macchiato wynosza: " + karta + " zl");
	            			iloscLatteMacchiato=iloscLatteMacchiato - 1;
	                        Kawa.add(" Ilosc Latte Macchiato w Automacie: " + iloscLatteMacchiato + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	            else if(dodatki==4)
	            {
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaLatteMacchiato+cenaMleka_Cukru)
	            		{
	            			gotowka=gotowka-cenaLatteMacchiato-cenaMleka_Cukru;            
	            			Kawa.add(" Reszta: " + gotowka + " zl");
	            			iloscLatteMacchiato=iloscLatteMacchiato - 1;
	                        Kawa.add(" Ilosc Latte Macchiato w Automacie: " + iloscLatteMacchiato + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaLatteMacchiato+cenaMleka_Cukru)
	            		{
	            			karta=karta-cenaLatteMacchiato-cenaMleka_Cukru;				            
	            			Kawa.add(" Srodki na kacie po zakupie Latte Macchiato wynosza: " + karta + " zl");
	            			iloscLatteMacchiato=iloscLatteMacchiato - 1;
	                        Kawa.add(" Ilosc Latte Macchiato w Automacie: " + iloscLatteMacchiato + "\n");
	                		Kawa.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	            }
	        }
	   
	    	else
	    		Kawa.add(" Brak Kawy, lub nie wybrano metody platnosci ");
	    	
	    	for(String s : Kawa) { System.out.println(s); }
	    		return gotowka;
	    }
	
		public double kupWode(int woda, int metoda, double gotowka) {
			return 0;
		}
		
		public double kupNapoj(int napoj, int metoda, double gotowka) {
			return 0;
		}
		
		public double kupHerbate(int herbata, int dodatki, int metoda, double gotowka) {
			return 0;
		}
		
		public double kupPrzekaski(int przekaski, int metoda, double gotowka) {
			return 0;
		}
		
		public double wciaganiePieniedzy(double gotowka) {
			return 0;
		}
}
