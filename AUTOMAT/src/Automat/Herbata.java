
package Automat;

import java.util.ArrayList;

public class Herbata extends Platnosc implements Produkty
{
    private int iloscHerbaty;
    private double cenaHerbaty;
    private int iloscHerbatyOwocowej;
    private double cenaHerbatyOwocowej;
        
    private double cenaCukru;
    private double cenaCytryny;
    private int cenaCytryny_Cukru;
        
        public Herbata() // konstruktor domyślny, bezparametrowy
        {
        	iloscHerbaty= 8;
            cenaHerbaty = 1.5;
            iloscHerbatyOwocowej= 6;
            cenaHerbatyOwocowej = 1.5;
            cenaCukru = 0.5;
            cenaCytryny = 0.5;
            cenaCytryny_Cukru = 1;
        }
        
        public Herbata(int iloscHerbaty, double cenaHerbaty, int iloscHerbatyOwocowej, double cenaHerbatyOwocowej, double cenaCukru, double cenaCytryny, int cenaCytryny_Cukru) // konstruktor przyjmujący jeden, lub wiele parametrow
        {
        	this.iloscHerbaty = iloscHerbaty;
            this.cenaHerbaty = cenaHerbaty;
            this.iloscHerbatyOwocowej = iloscHerbatyOwocowej;
            this.cenaHerbatyOwocowej = cenaHerbatyOwocowej;
            this.cenaCukru = cenaCukru;
            this.cenaCytryny = cenaCytryny;
            this.cenaCytryny_Cukru = cenaCytryny_Cukru;
        }
        
        public Herbata(Herbata herbata) // konstruktor przyjmujący obiekt herbata klasy Herbata jako parametr
        {
        	iloscHerbaty= herbata.iloscHerbaty;
            cenaHerbaty = herbata.cenaHerbaty;
            iloscHerbatyOwocowej= herbata.iloscHerbatyOwocowej;
            cenaHerbatyOwocowej = herbata.cenaHerbatyOwocowej;
            cenaCukru = herbata.cenaCukru;
            cenaCytryny = herbata.cenaCytryny;
            cenaCytryny_Cukru = herbata.cenaCytryny_Cukru;
        }
		
	    public final double kupHerbate(int herbata, int dodatki, int metoda, double gotowka) // metoda odpowiedzialna za kupowanie danego produktu
	    {  		
	    	ArrayList<String> Herbata = new ArrayList<String>();
	    	
	    	if(iloscHerbaty>0 && herbata==12 && metoda==1 || metoda==2)
	        {   
	    		if(dodatki==1)
	    		{
	                switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaHerbaty)
	            		{
	            			gotowka=gotowka-cenaHerbaty;            
	            			Herbata.add(" Reszta: " + gotowka + " zl");	
	                        iloscHerbaty=iloscHerbaty - 1;
	                        Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbaty + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaHerbaty)
	            		{
	            			karta=karta-cenaHerbaty;				            
	            			Herbata.add(" Srodki na kacie po zakupie Herbaty wynosza: " + karta + " zl");
	                        iloscHerbaty=iloscHerbaty - 1;
	                        Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbaty + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}  
	    		
	    		else if(dodatki==2)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaHerbaty+cenaCytryny)
	            		{
	            			gotowka=gotowka-cenaHerbaty-cenaCytryny;            
	            			Herbata.add(" Reszta: " + gotowka + " zl");	
	                    	iloscHerbaty=iloscHerbaty - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbaty + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaHerbaty+cenaCytryny)
	            		{
	            			karta=karta-cenaHerbaty-cenaCytryny;				            
	            			Herbata.add(" Srodki na kacie po zakupie Herbaty wynosza: " + karta + " zl");
	                    	iloscHerbaty=iloscHerbaty - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbaty + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	    		
	    		else if(dodatki==3)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaHerbaty+cenaCukru)
	            		{
	            			gotowka=gotowka-cenaHerbaty-cenaCukru;            
	            			Herbata.add(" Reszta: " + gotowka + " zl");	
	                    	iloscHerbaty=iloscHerbaty - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbaty + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaHerbaty+cenaCukru)
	            		{
	            			karta=karta-cenaHerbaty-cenaCukru;				            
	            			Herbata.add(" Srodki na kacie po zakupie Herbaty wynosza: " + karta + " zl");
	                    	iloscHerbaty=iloscHerbaty - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbaty + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	            else if(dodatki==4)
	            {
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaHerbaty+cenaCytryny_Cukru)
	            		{
	            			gotowka=gotowka-cenaHerbaty-cenaCytryny_Cukru;            
	            			Herbata.add(" Reszta: " + gotowka + " zl");		
	                    	iloscHerbaty=iloscHerbaty - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbaty + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaHerbaty+cenaCytryny_Cukru)
	            		{
	            			karta=karta-cenaHerbaty-cenaCytryny_Cukru;				            
	            			Herbata.add(" Srodki na kacie po zakupie Herbaty wynosza: " + karta + " zl");
	                    	iloscHerbaty=iloscHerbaty - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbaty + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	            }
	        }
	    	
	    	else if(iloscHerbatyOwocowej>0 && herbata==13 && metoda==1 || metoda==2)
	        {   
	    		if(dodatki==1)
	    		{
	                switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaHerbatyOwocowej)
	            		{
	            			gotowka=gotowka-cenaHerbatyOwocowej;            
	            			Herbata.add(" Reszta: " + gotowka + " zl");	
	            			iloscHerbatyOwocowej=iloscHerbatyOwocowej - 1;
	                        Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbatyOwocowej + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaHerbatyOwocowej)
	            		{
	            			karta=karta-cenaHerbatyOwocowej;				            
	            			Herbata.add(" Srodki na kacie po zakupie Herbaty wynosza: " + karta + " zl");
	            			iloscHerbatyOwocowej=iloscHerbatyOwocowej - 1;
	                        Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbatyOwocowej + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}  
	    		
	    		else if(dodatki==2)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaHerbatyOwocowej+cenaCytryny)
	            		{
	            			gotowka=gotowka-cenaHerbatyOwocowej-cenaCytryny;            
	            			Herbata.add(" Reszta: " + gotowka + " zl");	
	            			iloscHerbatyOwocowej=iloscHerbatyOwocowej - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbatyOwocowej + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaHerbatyOwocowej+cenaCytryny)
	            		{
	            			karta=karta-cenaHerbatyOwocowej-cenaCytryny;				            
	            			Herbata.add(" Srodki na kacie po zakupie Herbaty wynosza: " + karta + " zl");
	            			iloscHerbatyOwocowej=iloscHerbatyOwocowej - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbatyOwocowej + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	    		
	    		else if(dodatki==3)
	    		{
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaHerbatyOwocowej+cenaCukru)
	            		{
	            			gotowka=gotowka-cenaHerbatyOwocowej-cenaCukru;            
	            			Herbata.add(" Reszta: " + gotowka + " zl");	
	            			iloscHerbatyOwocowej=iloscHerbatyOwocowej - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbatyOwocowej + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaHerbatyOwocowej+cenaCukru)
	            		{
	            			karta=karta-cenaHerbatyOwocowej-cenaCukru;				            
	            			Herbata.add(" Srodki na kacie po zakupie Herbaty wynosza: " + karta + " zl");
	            			iloscHerbatyOwocowej=iloscHerbatyOwocowej - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbatyOwocowej + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	    		}
	            else if(dodatki==4)
	            {
	            	switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaHerbatyOwocowej+cenaCytryny_Cukru)
	            		{
	            			gotowka=gotowka-cenaHerbatyOwocowej-cenaCytryny_Cukru;            
	            			Herbata.add(" Reszta: " + gotowka + " zl");		
	            			iloscHerbatyOwocowej=iloscHerbatyOwocowej - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbatyOwocowej + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break; 
	            	case 2:
	            		if(karta>=cenaHerbatyOwocowej+cenaCytryny_Cukru)
	            		{
	            			karta=karta-cenaHerbatyOwocowej-cenaCytryny_Cukru;				            
	            			Herbata.add(" Srodki na kacie po zakupie Herbaty wynosza: " + karta + " zl");
	            			iloscHerbatyOwocowej=iloscHerbatyOwocowej - 1;
	                    	Herbata.add(" Ilosc Herbaty w Automacie: " + iloscHerbatyOwocowej + "\n");
	                		Herbata.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
	            }
	        }
	            
	        else
	        	Herbata.add(" Brak Herbaty, lub nie wybrano metody platnosci ");
	    	
	    	for(String s : Herbata) { System.out.println(s); }
	          	return gotowka;      
	    }
	
		public double kupWode(int woda, int metoda, double gotowka) {
			return 0;
		}
	
		public double kupNapoj(int napoj, int metoda, double gotowka) {
			return 0;
		}
		
		public double kupKawe(int kawa, int dodatki, int metoda, double gotowka) {
			return 0;
		}
		
		public double kupPrzekaski(int przekaski, int metoda, double gotowka) {
			return 0;
		}
		
		public double wciaganiePieniedzy(double gotowka) {
			return 0;
		}
}