
package Automat;

import java.util.ArrayList;

public class Gazowane extends Platnosc implements Produkty // klasa dziedziczy pole karta z klasy Platnosc
{
    private int iloscColi;
    private double cenaColi;
    private int iloscFanty;
    private double cenaFanty;
    private int iloscSprite;
    private double cenaSprite;
    
        public Gazowane() // konstruktor domyślny, bezparametrowy
        {
        	iloscColi = 6;
        	cenaColi = 2;
        	iloscFanty = 4;
        	cenaFanty = 2;
        	iloscSprite = 2;
        	cenaSprite = 2;
        }
        
        public Gazowane(int iloscColi, double cenaColi, int iloscFanty, double cenaFanty, int iloscSprite, double cenaSprite) // konstruktor przyjmujący jeden, lub wiele parametrow
        {
        	this.iloscColi = iloscColi;
        	this.cenaColi = cenaColi;
        	this.iloscFanty = iloscFanty;
        	this.cenaFanty = cenaFanty;
        	this.iloscSprite = iloscSprite;
        	this.cenaSprite = cenaSprite;
        }
        
        public Gazowane(Gazowane napoje) // konstruktor przyjmujący obiekt napoje klasy Gazowane jako parametr
        {
        	iloscColi = napoje.iloscColi;
        	cenaColi = napoje.cenaColi;
        	iloscFanty = napoje.iloscFanty;
        	cenaFanty = napoje.cenaFanty;
        	iloscSprite = napoje.iloscSprite;
        	cenaSprite = napoje.cenaSprite;
        }
        
        public final double kupNapoj(int napoj, int metoda, double gotowka) // metoda odpowiedzialna za kupowanie danego produktu
        {  	
        	ArrayList<String> Gazowane = new ArrayList<String>();

        	if(iloscColi>0 && napoj==1 && metoda==1 || metoda==2)
            {
        		switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaColi)
	            		{
	            			gotowka=gotowka-cenaColi;            
	            			Gazowane.add(" Reszta: " + gotowka + " zl");
	            			iloscColi=iloscColi - 1;
	            			Gazowane.add(" Ilosc Coli w Automacie: " + iloscColi + "\n");
	            			Gazowane.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break;
	            
	            	case 2:
	            		if(karta>=cenaColi)
	            		{
	            			karta=karta-cenaColi;				            
	            			Gazowane.add(" Srodki na kacie po zakupie Coli wynosza: " + karta + " zl");
	            			iloscColi=iloscColi - 1;
	            			Gazowane.add(" Ilosc Coli w Automacie: " + iloscColi + "\n");
	            			Gazowane.add("	Zakup udany. Dziekujemy ");
	            		}
	            		
	            	}
            }
               
        	else if(iloscFanty>0 && napoj==2 && metoda==1 || metoda==2)
            {
        		switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaFanty)
	            		{
	            			gotowka=gotowka-cenaFanty;            
	            			Gazowane.add(" Reszta: " + gotowka + " zl");
	            			iloscFanty=iloscFanty - 1;
	            			Gazowane.add(" Ilosc Fanty w Automacie: " + iloscFanty + "\n");
	            			Gazowane.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break;  
	            	case 2:
	            		if(karta>=cenaFanty)
	            		{
	            			karta=karta-cenaFanty;				            
	            			Gazowane.add(" Srodki na kacie po zakupie Fanty wynosza: " + karta + " zl");
	            			iloscFanty=iloscFanty - 1;
	            			Gazowane.add(" Ilosc Fanty w Automacie: " + iloscFanty + "\n");
	            			Gazowane.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
            }
        	
        	else if(iloscSprite>0 && napoj==3 && metoda==1 || metoda==2)
            {
        		switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaSprite)
	            		{
	            			gotowka=gotowka-cenaSprite;            
	            			Gazowane.add(" Reszta: " + gotowka + " zl");	
	            			iloscSprite=iloscSprite - 1;
	            			Gazowane.add(" Ilosc Sprite w Automacie: " + iloscSprite + "\n");
	            			Gazowane.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break;
	            	case 2:
	            		if(karta>=cenaSprite)
	            		{
	            			karta=karta-cenaSprite;				            
	            			Gazowane.add(" Srodki na kacie po zakupie Sprite wynosza: " + karta + " zl");
	            			iloscSprite=iloscSprite - 1;
	            			Gazowane.add(" Ilosc Sprite w Automacie: " + iloscSprite + "\n");
	            			Gazowane.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
            }
        	
            else
            	Gazowane.add(" Brak Napoju, lub nie wybrano metody platnosci ");
             
        	for(String s : Gazowane) { System.out.println(s); }
        		return gotowka;
        }

		public double kupWode(int woda, int metoda, double gotowka) {
			return 0;
		}

		public double kupKawe(int kawa, int dodatki, int metoda, double gotowka) {
			return 0;
		}

		public double kupHerbate(int herbata, int dodatki, int metoda, double gotowka) {
			return 0;
		}
		
		public double kupPrzekaski(int przekaski, int metoda, double gotowka) {
			return 0;
		}
		
		public double wciaganiePieniedzy(double gotowka) {
			return 0;
		}
}