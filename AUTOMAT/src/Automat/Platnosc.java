package Automat;

import java.util.ArrayList;
import java.util.Scanner;

public class Platnosc 
{
	protected double gotowka;
	protected double karta;

	Scanner in = new Scanner(System.in);
	
	    public Platnosc() // konstruktor domyślny, bezparametrowy
	    {
	    	this.gotowka = 0;
	    	this.karta = 100;
	    }
	    
	    public Platnosc(double gotowka, double karta) // konstruktor przyjmujący jeden, lub wiele parametrow
	    {
	    	this.gotowka = gotowka;
	    	this.karta = karta;
	    }    
	    
	    public Platnosc(Platnosc metoda) // konstruktor przyjmujący obiekt metoda klasy Platnosc jako parametr
	    {
	    	this.gotowka = metoda.gotowka;
	    	this.karta = metoda.karta;
	    }
	    
	    public static void menuPlatnosci() // metoda wyświetlając metody Platnosci 
		{ 	
	    	ArrayList<String> platnosci = new ArrayList<String>();
	    	
	    	platnosci.add("WYBIERZ METODE PLATNOSCI: ");
	    	platnosci.add(" 1 - Gotowka ");
	    	platnosci.add(" 2 - Karta ");
			
			for(String s : platnosci) { System.out.println(s); }
		}
	    
		public static void metodaPlatnosci(int metoda) // metoda wyswietlająca wybrane metody Platnosci
		{		
			ArrayList<String> metodaPlatnosci = new ArrayList<String>();
			
			switch(metoda)
				{
				case 1:
					metodaPlatnosci.add(" Wrzuc pieniadze: ");
					break;
				case 2:
					metodaPlatnosci.add(" Przyloz Karte, wcisnij 1 zatwierdz ENTER ");
					break;
			   default:
				    metodaPlatnosci.add("NIE WYBRANO METODY PLATNOSCI ");
				}
			
			for(String s : metodaPlatnosci)
			{
				System.out.println(s);
			}
		}
		
	    public double wciaganiePieniedzy(double gotowka) // metoda odpoweidzialana za wciąganie pieniędzy
	    {
	    	int wciaganie = (int)(Math.random()*10 + 1); // odwołanie do metody --> super.wciaganiePieniedzy(gotowka);
	    	
	    	if(wciaganie==2 || wciaganie==4 || wciaganie==6 || wciaganie==8)
	    	{
	    		this.gotowka = 0;
      	  		System.err.println(" AUTOMAT WCIĄGNĄŁ PIENIĄDZE !!! \n"); 
	    	}
	    	
	    	else
	    		System.out.println(" OK ");
	    	
	    	return this.gotowka;
	    }
	    
		// getter-y i setter-y ustawiające watrości z czytane z klawiatury 
	    public double pobierzGotowke(Scanner in){
	    	return this.in.nextDouble();
	    }
	    
	    public void ustawGotowke(Scanner in)
	    {
	    	this.in = in;
	    }
	    
	    public double pobierzKarta()
	    {
	    	return this.karta; 
	    }
	    
	    public void ustawKarta(double karta)
	    {
			this.karta = karta;
	    }
	    
	    public int pobierzMetodePlatnosci(Scanner in)
	    {
	    	return this.in.nextInt(); 
	    }
	    
	    public void ustawPlatnosc(Scanner in)
	    {
			this.in = in;
	    }
}
