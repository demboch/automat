
package Automat;

import java.util.ArrayList;

public class Woda extends Platnosc implements Produkty
{
	private int iloscWodyMineralnej;
    private double cenaWodyMineralnej;
    private int iloscWodyGazowanej;
    private double cenaWodyGazowanej;
       
        public Woda() // konstruktor domyślny, bezparametrowy
        {
        	iloscWodyMineralnej = 8;
            cenaWodyMineralnej = 1.5;
            iloscWodyGazowanej = 6;
            cenaWodyGazowanej = 1.5;
        }
        
        public Woda(int iloscWodyMineralnej, double cenaWodyMineralnej, int iloscWodyGazowanej, double cenaWodyGazowanej) // konstruktor przyjmujący jeden, lub wiele parametrow
        {
        	this.iloscWodyMineralnej = iloscWodyMineralnej;
            this.cenaWodyMineralnej = cenaWodyMineralnej;
            this.iloscWodyGazowanej = iloscWodyGazowanej;
            this.cenaWodyGazowanej = cenaWodyGazowanej;
        }
        
        public Woda(Woda woda) // konstruktor przyjmujący obiekt woda klasy Woda jako parametr
        {
        	iloscWodyMineralnej = woda.iloscWodyMineralnej;
        	cenaWodyMineralnej = woda.cenaWodyMineralnej;
        	iloscWodyGazowanej = woda.iloscWodyGazowanej;
        	cenaWodyGazowanej = woda.cenaWodyGazowanej;
        }
        
        public final double kupWode(int woda, int metoda, double gotowka) // metoda odpowiedzialna za kupowanie danego produktu
        {        
        	ArrayList<String> Woda = new ArrayList<String>();
        	
        	if(iloscWodyMineralnej>0 && woda==4 && metoda==1 || metoda==2)
            {
        		switch(metoda)
 	            	{
 	            	case 1: 
			            if(gotowka>=cenaWodyMineralnej)
			            {	
			            	super.wciaganiePieniedzy(super.gotowka);
			            	if(gotowka!=0)
			            	{
			            		
			            	gotowka=gotowka-cenaWodyMineralnej;            
				            Woda.add(" Reszta: " + gotowka + " zl");	
		        			iloscWodyMineralnej=iloscWodyMineralnej - 1;
		        			Woda.add(" Ilosc Wody Mineralnej w Automacie: " + iloscWodyMineralnej + "\n");
		        			Woda.add("	Zakup udany. Dziekujemy ");
			            	} 
			            	
			            	else if(gotowka==0)
			            	{
			            		System.out.println("zle");
			            	}
			            }
			            break;    
 	            	case 2:
 	            		if(karta>=cenaWodyMineralnej)
			            {
 	            			karta=karta-cenaWodyMineralnej;				            
 	            			Woda.add(" Srodki na kacie po zakupie Wody Mineralnej wynosza: " + karta + " zl");
 	            			iloscWodyMineralnej=iloscWodyMineralnej - 1;
 	            			Woda.add(" Ilosc Wody Mineralnej w Automacie: " + iloscWodyMineralnej + "\n");
 	            			Woda.add("	Zakup udany. Dziekujemy ");
			            }
 	            		break;
 	            	}
            }
        	
        	else if(iloscWodyGazowanej>0 && woda==5 && metoda==1 || metoda==2)
        	{
        		switch(metoda)
	            	{
	            	case 1:
	            		if(gotowka>=cenaWodyGazowanej)
	            		{
	            			gotowka=gotowka-cenaWodyGazowanej;            
	            			Woda.add(" Reszta: " + gotowka + " zl");	
	            			iloscWodyGazowanej=iloscWodyGazowanej - 1;
	            			Woda.add(" Ilosc Wody Gazowanej w Automacie: " + iloscWodyGazowanej + "\n");
	            			Woda.add("	Zakup udany. Dziekujemy ");
	            		} 
	            		break;   
	            	case 2:
	            		if(karta>=cenaWodyGazowanej)
	            		{
	            			karta=karta-cenaWodyGazowanej;				            
	            			Woda.add(" Srodki na kacie po zakupie Wody Gazowanej wynosza: " + karta + " zl");
	            			iloscWodyGazowanej=iloscWodyGazowanej - 1;
	            			Woda.add(" Ilosc Wody Gazowanej w Automacie: " + iloscWodyGazowanej + "\n");
	            			Woda.add("	Zakup udany. Dziekujemy ");
	            		}
	            		break;
	            	}
            }
            
        	else
        		Woda.add(" Brak Wody, lub nie wybrano metody platnosci ");
        	
        	for(String s : Woda) { System.out.println(s); }
               return gotowka;            
        }
	
		public double kupNapoj(int napoj, int metoda, double gotowka) {
			return 0;
		}

		public double kupKawe(int kawa, int dodatki, int metoda, double gotowka) {
			return 0;
		}

		public double kupHerbate(int herbata, int dodatki, int metoda, double gotowka) {
			return 0;
		}
		
		public double kupPrzekaski(int przekaski, int metoda, double gotowka) {
			return 0;
		}
		
		public double wciaganiePieniedzy(double gotowka) {
			return 0;
		}
}
